## Tangelov-me

![TravisCI status](https://travis-ci.org/tangel0v/tangelov-me.svg?branch=master)
![Gitlab Pipelines status](https://gitlab.com/tangelov/tangelov-me/badges/master/pipeline.svg)

This repository will store all the code assets needed to recreate a blog like tangelov.me.

### Installation and configuration
This project has some prerrequisites to be built:

* python 3
* pip
* git
* gcloud 

First we have to clone the repository and get the code:

```bash
git clone https://gitlab.com/tangelov/tangelov-me.git
cd tangelov-me
```

Second we have to install the requirements:

```bash
python3 -m venv virtualenv && source virtualenv/bin/activate
pip3 install -r requirements.txt
```

Finally we create the static code

```bash
nikola build
```

If you wish to deploy the code in a container, we provide a Dockerfile

```bash
docker build . -t tangelov-me
```
